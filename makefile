parentheses3v2: obj/parentheses3v2.o obj/affichage.o
	ld $^ -o $@

obj/parentheses3v2.o: src/parentheses3v2.s
	as $^ --gstabs -o $@

obj/affichage.o: src/affichage.s
	as $^ --gstabs -o $@

doc: doc/algo.tex
	./doc/make_pdf_doc.sh

clean:
	rm -rf obj/*.o 
