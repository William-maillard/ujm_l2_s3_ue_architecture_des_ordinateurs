	.data
message:		.string "     Paires de parentheses :\n\0"		         #taille: 30
tirets:			.string "     -----------------------\n\0"		         #taille: 30
tabulation:		.string "              \0"					             #taille: 6
oui:			.string "     Analyse : OUI\n\0"			             #taille: 20
non_fermante:		.string "     Analyse : NON, fermante en trop\n\0" 	 #taille: 38
non_ouvrante:		.string "     Analyse : NON, ouvrante en trop\n\0" 	 #taille: 38
invalide:		.string "Il faut au moins 2 arguments !!\n\0"		     #taille: 33
mauvais_parenthesage:	.string "     Analyse : NON, X en sommet de pile alors que je lis Y\n\0" #taille: 59, X position 20 et Y position 57 a remplacer par les parenthes lus sur la pile et dans la chaine.
parentheses:		.string "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0" #taille: 21
saut:			.string "\n\0"

	.text
	.global _start

_start:
	pop %r8		 	 	    #R8=argc
	cmp $3, %r8		 	    #(R8<3)?
	JL afficher_invalide	#si vraie alors le programme a ete appele avec 0 ou 1 argument
	pop %rdx		 	    #RDX=argv[0], nom du programme
	xor %r10, %r10			#R10=0, utilise pour se deplacer dans parentheses
	mov $message, %rsi		#RSI= *message
	call affiche_chaine		#affiche la chaine dont l'adresse de debut se trouve dans RSI
	mov $tirets, %rsi		#RSI= *tirets
	call affiche_chaine		

tq_paires_parentheses:
	cmp $2, %r8		 		#(R8=2)?
	JE fin_tq_pp			#si vraie, alors on a recupere toutes les paires de parentheses placees en argument, on passe a la suite
afficher_paires:
	mov $tabulation, %rsi	#on affiche une tabulation
	call affiche_chaine
	pop %rsi		 		#rsi= l'adresse de la chaine contenant une pair de parenthese place en argument du programme
	call affiche_chaine		#on affiche la paire de parentheses
stocke_paire_dans_parentheses:	
	movb (%rsi), %al		    #AL= parentheses ouvrante
	movb %al,parentheses(%r10)	#on stocke la parenthese ouvrante de AL dans la chaine parentheses a la position RCX
	inc %r10				    #R10++, on se place au caractere suivant dans la chaine
	inc %rsi				    #RSI++, on passe a la parenthese suivante
	movb (%rsi),%al				#AL= parenthese fermante
	mov %rax,parentheses(%r10)	#on stocke la parenthese fermante de AL dans la chaine parentheses a la position RCX
	inc %r10				    #RCX++, on se place au caractere suivant dans la chaine
	dec %r8					    #R8--, car on a traite 1 paire de parentheses
fin_affichage:
	mov $saut, %rsi
	call affiche_chaine			#on affiche un saut de ligne
	JMP tq_paires_parentheses	#tour de boucle suivant

fin_tq_pp:
	mov $saut, %rsi
	call affiche_chaine			#on affiche un saut de ligne
	pop %rsi				    #RSI= *argv[argc-1]
	push $0					    #on met 0 en fond de pile, pour savoir quand elle est vide
	
tant_que:
	xor %r10, %r10				#R10=0, met l'indice pour parcourir parentheses a 0
	movb (%rsi), %al			#AL= le caractere de la chaine sur lequel pointe RSI
	test %al, %al				#(al=0)?, est-on en fin de chaine
	JZ fin_tq

test_ouvrante:
	movb parentheses(%r10), %bl	#BL= le R10° caractere de parentheses
	test %bl, %bl				#(BL=0)?
	JZ ini_test_fermante		#si vraie alors on a parcouru toutes les parentheses ouvrantes de parentheses sans trouver de correspondance.
	cmp %bl, %al				#(AL= BL)?
	JE ouvrante				    #si vraie AL est la R10° parenthese ouvrante de parentheses
	add $2, %r10				#R10 contient l'adresse d'une parenthese ouvrantes dans parentheses (situe a des adresses pair)
	JMP test_ouvrante			#on test si AL est la parenthese ouvrante suivante

ouvrante:
	push %rax				   #AL est une parenthese ouvrante, on l'empile
	inc %rsi				   #on pase au caractere suivant
	JMP tant_que

ini_test_fermante:
	mov $1, %r10				#R10=1, car les parentheses fermantes sont a un indice impaire

test_fermante:
	movb parentheses(%r10), %bl	#BL= le R10° caractere de parentheses
	test %bl, %bl				#(BL=0)?
	JZ nouveau_tour				#si vraie alors on a parcouru toutes les parentheses fermantes de parentheses sans trouver de correspondance
						        #donc Al contient un carctere quelconque, on va traiter le caractere suivant
	cmp %bl, %al				#(AL= BL)?
	JE fermante				    #alors AL contient une parenthese fermante
	add $2, %r10				#R10 contient l'adresse d'une parenthese fermantes dans parentheses (situe a des adresses impaire)
	JMP test_fermante			#on va tester si Al est la parenthese suivante fermante

fermante:
	dec %r10				   #R10-- pour selectionner la parenthese complementaire a la parenthese fermante se trouvant dans AL
	pop %rbx				   #RBX= sommet de pile
	test %rbx, %rbx			   #(RBX=0)?
	JZ afficher_non_fermante   #si vraie alors on a une fermante en trop
	cmp parentheses(%r10), %bl #sinon on test si c'est la parenthese complementaire a al
	JE nouveau_tour			   #si vraie alors on a ferme la pair en depilant l'ouvrante on passe au carctere suivant			
	JNE afficher_mauvais_parenthesage	#sinon, la chaine est mal parenthesee

nouveau_tour:
	inc %rsi				   #on fait pointer rsi sur le caractere suivant
	JMP tant_que
	
fin_tq:
	pop %rbx				   #RBX= sommet de pile
	test %rbx,%rbx			   #(RBX=0)?
	JNZ afficher_non_ouvrante  #si faux, alors, on a une parenthese qui na pas ete ferme
	JMP afficher_oui		   #si vraie, alors la chaine est bien parenthese

	
afficher_mauvais_parenthesage:
	mov $20, %r10					     #R10=numero du char X dans la chaine mauvais_parenthesage
	movb %bl, mauvais_parenthesage(%r10) #On remplace X dans mauvais_parenthesage par le caractere se trouvant au somment de la pile que l'on a push dans RBX (BL est son octet de poids faible)
	mov $57, %r10					     #R10=numero du char Y dans la chaine mauvais_parenthesage	
	movb %al, mauvais_parenthesage(%r10) #On remplace Y dans notre message par le caractere que l'on etait en train de traiter (AL)
	movq $mauvais_parenthesage, %rsi	 #RSI<- (la chaine a afficher)
	call affiche_chaine				     #on affiche l'analyse	
	JMP exit					         #fin


afficher_invalide:
	movq $invalide, %rsi	    #RSI<- (la chaine a afficher) 
	call affiche_chaine		    #on l'affiche	
	JMP exit                    #fin
	
afficher_oui:
	movq $oui, %rsi		  	    #RSI<- (la chaine a afficher)
	call affiche_chaine		    #on l'affiche
	JMP exit			        #fin

afficher_non_ouvrante:
	movq $non_ouvrante, %rsi	#RSI<- (la chaine a afficher)
	call affiche_chaine		    #on l'affiche
	JMP exit			        #fin

afficher_non_fermante:
	movq $non_fermante, %rsi	#RSI<- (la chaine a afficher)
	call affiche_chaine		    #on l'affiche
	JMP exit			        #fin

exit:
	movq $60, %rax			   #RAX=60, numero de exit
	xorq %rdi, %rdi			   #RDI=0
	syscall				       #appel systeme
