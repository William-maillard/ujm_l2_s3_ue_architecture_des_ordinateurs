# module du cours permettant d'afficher une chaine dont l'adresse de debut se trouve dans RSI
#utilise les registre: RDX, RBX, RAX, RDI

	.text
	.global affiche_chaine

affiche_chaine:
	xor %rdx, %rdx		# RDX=0
	mov %rsi, %rbx		# RBX=RSI
boucle:
	movb (%rbx), %al	# AL= *RBX
	test %al,%al		# AL == 0?, fin de la chaine?
	JZ fin			# alors fini
	inc %rdx		# RDX++, longueur de la chaine +1
	inc %rbx		# RBX++, caractere suivant
	JMP boucle
fin:				# write
	mov $1,%rax		# RAX=1 (numero de write)
	mov $1,%rdi		# RDI=1 (ecrit sur stdout)
	syscall			# appel systeme
	ret			# retour
