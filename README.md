# UJM_L2_S3_UE_ARCHITECTURE_DES_ORDINATEURS

Réalisation d'un programme qui vérifie qu'une chaine de parenthèses est bien parenthésé en assembleur x86.
L'utilisateur peut définir les paires de parentthèses avec n'importe quel charactères au début du programme.

## Table des matières

- [UJM_L2_S3_UE_ARCHITECTURE_DES_ORDINATEURS](#ujm_l2_s3_ue_architecture_des_ordinateurs)
- [Sujet](#sujet)
- [Algorithme](#algorithme)
- [Installation](#installation)
- [Utilisation](#utilisation)
  - [Exemples](#exemples)
- [Docker](#docker)
  - [Récupération de l'image](#récupération-de-limage)
  - [Exemple d'utilisation](#exemple-dutilisation)

## Sujet

![Le projet](/doc/sujet.png)

## Algorithme

![Le projet](/doc/algorithme.png)

## Installation

- Cloner le répot et réaliser un make
- ou installer l'exécutable ['parentheses3v2'](/download/parentheses3v2)
 fournis
- ou importer l'image docker du projet

## Utilisation

Exécuter le programe comme suit, en modifiant les paramètres conformément à son utilisation:

```sh
./parentheses3v2 p1 p2 ... p10 chaineAverifier
```

avec :

- p1 ... p1O : un nombre variable de couple de parenthèse différentes, par exemple : ab, (), "#, ui; mais pas ""
- chaineAverifier : une chaine de caractères contenant les couple de parenthèses précédament définies, pour vérifier qu'elles soient bien utilisées.

## Exemples

```sh
$ ./parentheses3v2
Il faut au moins 2 arguments !!
```

```sh
$ ./parentheses3v2 "()" "(oui)a"
     Paires de parentheses :
     -----------------------
              ()

     Analyse : OUI
```

```sh
 $ ./parentheses3v2 'ab' 'yh' '60' '6gyhyjujhgyabh0jki'
     Paires de parentheses :
     -----------------------
              ab
              yh
              60

     Analyse : OUI
```

```sh
$ ./parentheses3v2 'ab' 'yh' '60' 'kiolynnap6ol0iymlpbbh'
     Paires de parentheses :
     -----------------------
              ab
              yh
              60

     Analyse : NON, y en sommet de pile alors que je lis b
```

## Docker

### récupération de l'image

```sh
─$ docker pull williammaillard/parentheses3v2
Using default tag: latest
latest: Pulling from williammaillard/parentheses3v2
edaedc954fb5: Already exists
83ea17720cd1: Already exists
4f4fb700ef54: Already exists
Digest: sha256:40f998fdb2181acbe8520d979b3cc53532b3f842702b577ec536fd0374366660
Status: Downloaded newer image for williammaillard/parentheses3v2:latest
docker.io/williammaillard/parentheses3v2:latest
```

si l'image est rendue indisponible sous docker hub il est
possible de l'installer [ici](/download/parentheses3v2docker.tar) et de l'importer :

```sh
docker load -i parentheses3v2docker.tar
```

### Exemple d'utilisation

```sh
$ docker run williammaillard/parentheses3v2 ab cd ef aceatestbfdb
     Paires de parentheses :
     -----------------------
              ab
              cd
              ef

     Analyse : NON, e en sommet de pile alors que je lis b
```

```sh
$ docker run -tti williammaillard/parentheses3v2 "ab" "aubaaabbeeb"
     Paires de parentheses :
     -----------------------
              ab

     Analyse : OUI

```
