#-----------#
#   Build   #
#-----------#
FROM ubuntu:20.04 AS build

# install required packages
RUN apt-get update &&\
    apt install -y make binutils

# cpoy source files
COPY src /app/src
COPY obj /app/obj
COPY makefile /app/makefile

# compilation
WORKDIR /app
RUN make parentheses3v2


#-----------#
#   Prod    #
#-----------#
FROM  ubuntu:20.04 AS prod

# get the executable from the build stage
COPY --from=build /app/parentheses3v2 /app/parentheses3v2

# Exécuter le programme
WORKDIR /app
ENTRYPOINT ["./parentheses3v2"]
CMD [] 
# to get arguments from the comand line of docker run parentheses arg1 ... argn